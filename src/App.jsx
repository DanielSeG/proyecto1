import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import ComponenteOne from './Components/ComponenteOne'
import { ComponenteDos } from './Components/ComponenteDos'

function App() {
  

  return (
    <div>
    <h1>Mi presentación</h1>
    <ul>Nombre: Jose Daniel</ul>
    <ul>Apellidos: Sedeño Garcia</ul>
    <ul>Carrera: Sistemas computacionales</ul>
    <ul>Semestre: 8</ul>
    <ComponenteOne/>
    <ComponenteDos/>
    </div>

    
  )
}

export default App
